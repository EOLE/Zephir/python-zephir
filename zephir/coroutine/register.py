from functools import wraps

AS_COROUTINE_TAG = 'as_coroutine'

def register(as_thread=False):
    def decorator(func):
        @wraps(func)
        async def wrapper(*args, **kwargs):
            if not as_thread:
                return await func(*args, **kwargs)
            else:
                coroutine_config = wrapper.__dict__[AS_COROUTINE_TAG]
                loop = coroutine_config['loop']
                return await loop.run_in_executor(None, func, *args, **kwargs)
        coroutine_config = {
            'loop': None,
            'instance': None,
            'method': None,
        }
        wrapper.__dict__[AS_COROUTINE_TAG] = coroutine_config
        return wrapper
    return decorator

def run_registered_coroutines(controller, loop):
    for key in dir(controller):
        attr = getattr(controller, key)
        if hasattr(attr, '__dict__') and AS_COROUTINE_TAG in attr.__dict__:
            coroutine_config = attr.__dict__[AS_COROUTINE_TAG]
            coroutine_config['loop'] = loop
            loop.run_until_complete(getattr(controller, key)())
