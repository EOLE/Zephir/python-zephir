# -*- coding: utf-8 -*-
import yaml
from os import listdir
from os.path import join, isfile
from collections import OrderedDict
from ..config import MESSAGE_ROOT_PATH
from ..i18n import _


class MessageDefinition:
    """
    A MessageDefinition is a representation of a message in the Zephir application messaging context
    """
    __slots__ = ('version',
                 'uri',
                 'description',
                 'sampleuse',
                 'domain',
                 'parameters',
                 'public',
                 'errors',
                 'pattern',
                 'related',
                 'response')

    def __init__(self, raw_def):
        # default value for non mandatory key
        self.version = u''
        self.parameters = OrderedDict()
        self.public = False
        self.errors = []
        self.related = []
        self.response = None
        self.sampleuse = None

        # loads yaml information into object
        for key, value in raw_def.items():
            if isinstance(value, str):
                value = value.strip()
            if key == 'public':
                if not isinstance(value, bool):
                    raise ValueError(_("{} must be a boolean, not {}").format(key, value))
            elif key == 'pattern':
                if value not in ['rpc', 'event', 'error']:
                    raise Exception(_('unknown pattern {}').format(value))
            elif key == 'parameters':
                if 'type' in value and isinstance(value['type'], str):
                    # should be a customtype
                    value = customtypes[value['type']].properties
                else:
                    value = _parse_parameters(value)
            elif key == 'response':
                value = ResponseDefinition(value)
            elif key == 'errors':
                value = _parse_error_definition(value)
            setattr(self, key, value)
        # check mandatory keys
        for key in self.__slots__:
            try:
                getattr(self, key)
            except AttributeError:
                raise Exception(_('mandatory key not set {} message').format(key))
        # message with pattern = error must be public
        if self.public is False and self.pattern == 'error':
            raise Exception(_('Error message must be public : {}').format(self.uri))


class ParameterDefinition:
    __slots__ = ('name',
                 'type',
                 'description',
                 'help',
                 'default',
                 'ref',
                 'shortarg')

    def __init__(self, name, raw_def):
        self.name = name
        # default value for non mandatory key
        self.help = None
        self.ref = None
        self.shortarg = None

        # loads yaml information into object
        for key, value in raw_def.items():
            if isinstance(value, str):
                value = value.strip()
            if key == 'type':
                if value.startswith('[]'):
                    tvalue = value[2:]
                else:
                    tvalue = value
                if tvalue in customtypes:
                    if value.startswith('[]'):
                        value = '[]{}'.format(customtypes[tvalue].type)
                    else:
                        value = customtypes[value].type
                else:
                    self._valid_type(value)
                #self._valid_type(value)
            setattr(self, key, value)
        # check mandatory keys
        for key in self.__slots__:
            try:
                getattr(self, key)
            except AttributeError:
                if key != 'default':
                    raise Exception(_('mandatory key not set "{}" in parameter').format(key))

    def _valid_type(self, typ):
        if typ.startswith('[]'):
            self._valid_type(typ[2:])
        elif typ not in ['Boolean', 'String', 'Number', 'File', 'Dict']:
            raise Exception(_('unknown parameter type: {}').format(typ))

class ResponseDefinition:
    """
    An ResponseDefinition is a representation of a response in the Zephir application messaging context
    """
    __slots__ = ('description',
                 'type',
                 'ref',
                 'parameters',
                 'required')

    def __init__(self, responses):
        self.ref = None
        self.parameters = None
        self.required = []
        for key, value in responses.items():
            if key in ['parameters', 'required']:
                raise Exception(_('parameters and required must be set with a custom type'))
            elif key == 'type':
                if value.startswith('[]'):
                    tvalue = value[2:]
                else:
                    tvalue = value
                if tvalue in customtypes:
                    self.parameters = customtypes[tvalue].properties
                    self.required = customtypes[tvalue].required
                    if value.startswith('[]'):
                        value = '[]{}'.format(customtypes[tvalue].type)
                    else:
                        value = customtypes[value].type
                else:
                    self._valid_type(value)
            setattr(self, key, value)
        # check mandatory keys
        for key in self.__slots__:
            try:
                getattr(self, key)
            except AttributeError:
                raise Exception(_('mandatory key not set {}').format(key))

    def _valid_type(self, typ):
        if typ.startswith('[]'):
            self._valid_type(typ[2:])
        elif typ not in ['Boolean', 'String', 'Number', 'File', 'Dict']:
            raise Exception(_('unknown parameter type: {}').format(typ))


class ErrorDefinition:
    """
    An ErrorDefinition is a representation of an error in the Zephir application messaging context
    """
    __slots__ = ('uri',)

    def __init__(self, raw_err):
        extra_keys = set(raw_err) - set(self.__slots__)
        if extra_keys:
            raise Exception(_('extra keys for errors: {}').format(extra_keys))
        self.uri = raw_err['uri']


def _parse_error_definition(raw_defs):
    new_value = []
    for raw_err in raw_defs:
        new_value.append(ErrorDefinition(raw_err))
    return new_value


def _parse_parameters(raw_defs):
    parameters = OrderedDict()
    for name, raw_def in raw_defs.items():
        parameters[name] = ParameterDefinition(name, raw_def)
    return parameters


def parse_definition(filename: str):
    return MessageDefinition(yaml.load(filename))

def is_message_defined(uri):
    version, message = split_message_uri(uri)
    path = get_message_file_path(version, message)
    return isfile(path)

def get_message(uri):
    load_customtypes()
    try:
        version, message = split_message_uri(uri)
        path = get_message_file_path(version, message)
        with open(path, "r") as message_file:
            message_content = parse_definition(message_file.read())
        message_content.version = version
        return message_content
    except Exception as err:
        import traceback
        traceback.print_exc()
        raise Exception(_('cannot parse message {}: {}').format(uri, str(err)))

def split_message_uri(uri):
    return uri.split('.', 1)

def get_message_file_path(version, message):
    return join(MESSAGE_ROOT_PATH, version, 'messages', message + '.yml')

def list_messages():
    messages = listdir(MESSAGE_ROOT_PATH)
    messages.sort()
    for version in messages:
        for message in listdir(join(MESSAGE_ROOT_PATH, version, 'messages')):
            if message.endswith('.yml'):
                yield version + '.' + message.rsplit('.', 1)[0]

class CustomParam:
    __slots__ = ('name',
                 'type',
                 'description',
                 'ref',
                 'default')

    def __init__(self, name, raw_def, required):
        self.name = name
        self.ref = None
        if name not in required:
            self.default = None

        # loads yaml information into object
        for key, value in raw_def.items():
            if isinstance(value, str):
                value = value.strip()
            if key == 'type':
                value = self._convert_type(value, raw_def)
            elif key == 'items':
                continue
            setattr(self, key, value)

        # check mandatory keys
        for key in self.__slots__:
            try:
                getattr(self, key)
            except AttributeError:
                # default value for non mandatory key
                if key != 'default':
                    raise Exception(_('mandatory key not set "{}" in parameter').format(key))

    def _convert_type(self, typ, raw_def):
        types = {'boolean': 'Boolean',
                 'string': 'String',
                 'number': 'Number',
                 'object': 'Dict',
                 'array': 'Array',
                 'file': 'File'}

        if typ not in list(types.keys()):
            # validate after
            return typ
        if typ == 'array' and 'items' in raw_def:
            if not isinstance(raw_def['items'], dict):
                raise Exception(_('items must be a dict'))
            if list(raw_def['items'].keys()) != ['type']:
                raise Exception(_('items must be a dict with only a type'))
            return '[]{}'.format(self._convert_type(raw_def['items']['type'], raw_def))
        return types[typ]


def _parse_custom_params(raw_defs, required):
    parameters = OrderedDict()
    for name, raw_def in raw_defs.items():
        parameters[name] = CustomParam(name, raw_def, required)
    return parameters


class CustomType:
    __slots__ = ('title',
                 'type',
                 'description',
                 'ref',
                 'properties',
                 'required')

    def __init__(self, raw_def):
        # default value for non mandatory key
        self.ref = None

        # loads yaml information into object
        for key, value in raw_def.items():
            if isinstance(value, str):
                value = value.strip()
            if key == 'type':
                value = self._convert_type(value, raw_def)
            elif key == 'properties':
                value = _parse_custom_params(value, raw_def.get('required', {}))

            setattr(self, key, value)
        # check mandatory keys
        for key in self.__slots__:
            try:
                getattr(self, key)
            except AttributeError:
                raise Exception(_('mandatory key not set "{}" in parameter').format(key))

    def _convert_type(self, typ, raw_def):
        types = {'boolean': 'Boolean',
                 'string': 'String',
                 'number': 'Number',
                 'object': 'Dict',
                 'array': 'Array'}

        if typ not in list(types.keys()):
            # validate after
            return typ
        if typ == 'array' and 'items' in raw_def:
            if not isinstance(raw_def['items'], dict):
                raise Exception(_('items must be a dict'))
            if list(raw_def['items'].keys()) != ['type']:
                raise Exception(_('items must be a dict with only a type'))
            return '[]{}'.format(self._convert_type(raw_def['items']['type'], raw_def))
        return types[typ]

    def getname(self):
        return self.title


customtypes = {}
def load_customtypes():
    if not customtypes:
        versions = listdir(MESSAGE_ROOT_PATH)
        versions.sort()
        for version in versions:
            for message in listdir(join(MESSAGE_ROOT_PATH, version, 'types')):
                if message.endswith('.yml'):
                    path = join(MESSAGE_ROOT_PATH, version, 'types', message)
                    message = message.rsplit('.', 1)[0]
                    with open(path, "r") as message_file:
                        try:
                            ret = CustomType(yaml.load(message_file))
                            customtypes[ret.getname()] = ret
                        except Exception as err:
                            import traceback
                            traceback.print_exc()
                            raise Exception('{} for {}'.format(err, message))
        for customtype in customtypes.values():
            properties = {}
            for key, value in customtype.properties.items():
                type_ = value.type
                if type_.startswith('[]'):
                    ttype_ = type_[2:]
                else:
                    ttype_ = type_
                if ttype_ in customtypes:
                    if type_.startswith('[]'):
                        raise Exception(_('cannot have []CustomType'))
                    properties[key] = customtypes[ttype_]
                else:
                    properties[key] = value
            customtype.properties = properties
