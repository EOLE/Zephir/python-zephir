from aiohttp import web, ClientSession
from json import loads, dumps
from os.path import isdir, isfile
from autobahn.wamp.exception import ApplicationError
from psycopg2.extras import DictCursor
from jwt import decode, get_unverified_header

from ..i18n import _
from ..config import DEBUG
from ..util import undefined, parse_function, build_arguments
from ..message import get_message, is_message_defined
UNKNOWN_ERROR_URI = 'zephir.unknown_error'

def fake_static_file():
    pass

class aiohttp_wrapper:
    """this wrapper inject 'self' to function launch by aiohttp
    it returns answer as a aiohttp Response
    """
    httphandlers = []
    wamp_messages = set()
    http_messages = set()
    database = False
    message_parameters = None
    parameters_type = None
    add_uri_in_response = False
    func_args = None

    async def __new__(cls, request):
        if cls.method == 'get_file':
            return await cls.get_file(cls, request)
        elif cls.method == 'static':
            return await cls.static(cls, request)
        elif cls.method in ['get', 'post', 'put']:
            return await cls.http_method(cls, request, cls.method)
        else:
            raise ValueError(_('unknown method {}').format(cls.method))


    async def get_kwargs(self, request, uri):
        #if raises, kwargs must be defined
        kwargs = {}
        try:
            if self.parameters_type == 'url':
                kwargs = request.match_info
            elif self.parameters_type == 'json':
                kwargs = await request.json()
            else:
                raise Exception(_('unknown paramaters_type {}').format(self.paramaters_type))
            if kwargs is None:
                kwargs = {}
        except Exception as err:
            if DEBUG:
                import traceback
                traceback.print_exc()
                print('error when calling http - uri: {}, arguments: {}'.format(uri, kwargs))
            raise web.HTTPNotFound(reason=str(err))
        if self.add_uri_in_response:
            kwargs['uri'] = self.uri
        
        if 'X-Userinfo' in request.headers:
            jwt = request.headers['X-Userinfo']
            jwt += '.Gc==.Gc=='           
            kwargs['_session_user'] = {"username": get_unverified_header(jwt)["preferred_username"]} 
            for profil in decode(jwt, verify=False)['realm_access']['roles']:            
                if profil == "offline_access" or profil == "uma_authorization":
                    pass
                else:
                    kwargs['_session_user']['profil'] = profil
                    break                         
            
        elif 'X-Userinfo' in request.cookies:
            jwt = request.cookies['X-Userinfo']
            kwargs['_session_user'] = {"username": decode(jwt, verify=False)["preferred_username"]}
            for profil in decode(jwt, verify=False)['realm_access']['roles']:                
                if profil == "offline_access" or profil == "uma_authorization":
                    pass
                else:
                    kwargs['_session_user']['profil'] = profil
                    break
                    
        elif 'Authorization' in request.headers:
            jwt = request.headers['Authorization']
            if not jwt.startswith("Bearer "):
                raise Exception('unexpected bearer format')
            jwt = jwt[7:]        
            kwargs['_session_user'] = {"username": decode(jwt, verify=False)["preferred_username"]}
            for profil in decode(jwt, verify=False)['realm_access']['roles']:                
                if profil == "offline_access" or profil == "uma_authorization":
                    pass
                else:
                    kwargs['_session_user']['profil'] = profil
                    break

        elif DEBUG and "Referer" in request.headers:
            if "/messages/" in request.headers['Referer']:
                kwargs['_session_user'] = {"username": "PUBLIC", "profil": "root"}
        elif uri == "/v1":
            kwargs['_session_user'] = {"username": "PUBLIC", "profil": "VISITOR"}
        else:
            raise Exception('unknown user')
        return kwargs

    async def get_file(self, request):
        secret = request.match_info.get('secret')
        if secret is None:
            raise web.HTTPNotFound(reason=_('secret is mandatory for request "{}" and param "{}"').format(self.uri, self.param))
        if secret not in self.injected_self.http_sessions:
            raise web.HTTPNotFound(reason=_("unknown secret key {}").format(secret))
        args = []
        orig_secret = self.injected_self.http_sessions.pop(secret)
        if self.database:
            conn = await self.injected_self.connect_db()
            try:
                with conn.cursor(cursor_factory=DictCursor) as cursor:
                    data = await self.func(self.injected_self, cursor, orig_secret)
            except Exception as err:
                conn.rollback()
                raise web.HTTPNotFound(reason=str(err))
            else:
                conn.commit()
        else:
            data = await self.func(self.injected_self, orig_secret)
        if not isinstance(data, bytes):
            raise web.HTTPNotFound(reason=_("function return data which is not a bytes"))
        if DEBUG:
            print('call http - uri: {}, param: {}, secret: {}'.format(self.uri, self.param, secret))
        response = web.StreamResponse()
        response.type='application/octet-stream'
        response.content_length = len(data)
        await response.prepare(request)
        await response.write(data)
        return response

    def _dataresponse(self, data):
        content_type = None
        if isinstance(data, web.Response):
            return data
        if isinstance(data, (list, dict)):
            data = dumps(data)
            content_type = 'application/json'
        return web.Response(text=data, content_type=content_type)

    async def http_method(self, request, method):
        if DEBUG:
            print('call http {} - path: {}'.format(method, self.uri))

        try:

            kwargs = await self.get_kwargs(self, request, self.uri)
            kwargs = build_arguments(self.uri, self.message_parameters, self.func_args, kwargs, self.add_uri_in_response)

            if DEBUG:
                print('call http: {}, arguments: {}'.format(self.uri, kwargs))

            data = await self.func(self.injected_self, request, **kwargs)

        except Exception as err:

            import traceback
            traceback.print_exc()

            if isinstance(err, ApplicationError):
                return self.api_error(uri=err.error, kwargs=err.kwargs)
            else:
                return self.api_error(uri=UNKNOWN_ERROR_URI, kwargs={ "reason": str(err) if DEBUG else _("unknown error") })

        return self._dataresponse(self, data)

    async def static(self, request):
        if DEBUG:
            print('call http - static file: {}'.format(self.uri))
        return web.FileResponse(self.param)

    def api_error(uri=None, kwargs=None):
        if is_message_defined(uri):
            message = get_message(uri)
            description = _(message.description)
        else:
            description = _('unknown error')
        return web.Response(
                body=dumps({
                  'error': {
                    'uri': uri,
                    'kwargs': kwargs,
                    'description': description
                  }
                }).encode('utf-8'),
                status=500,
                content_type='application/json')

def register(uri: str,
             param: str=undefined,
             database: bool=False,
             http_type: str='message',
             parameters_type: str='url',
             add_uri_in_response: bool=False,
             allow_kwargs=False):

    if database:
        aiohttp_wrapper.database = True
    def decorator(func):
        lparam = None
        if http_type == 'message':
            # http_type message is used when a parameter needs to transfert with http
            # not WAMP
            message_content = get_message(uri)
            # only secret is allowed here
            func_args = parse_function(func, ('secret',), database)
            if message_content.response:
                message_parameters = message_content.response.parameters
            else:
                message_parameters = {}
            if message_content.pattern != 'rpc':
                raise TypeError(_('param only usable for rpc pattern for {}').format(uri))
            for message_param, options in message_parameters.items():
                if param == message_param:
                    aiohttp_wrapper.http_messages.add((uri, message_param))
                    break
            else:
                raise ValueError(_('param {} is not a valid parameter for {}').format(param, uri))
            if options.type != 'File':
                raise ValueError(_('only File parameter is allow for {}, not {} as {}').format(uri, options.type, param))
            path = '/' + uri + '/' + param + '/{secret}'
            method = 'get_file'
            message_content = get_message(uri)
            cls_message_parameters = message_content.parameters
        else:
            if uri.startswith('/'):
                # it's not an URI but directly the path
                path = uri
                cls_message_parameters = None
                if http_type == 'static' and not isdir(func) and isfile(func):
                    lparam = func
                    func = fake_static_file
            else:
                # it's an URI, useful mostly for api-bridge
                message_content = get_message(uri)
                #if message_content.response:
                #    message_parameters = message_content.response.parameters
                #else:
                #    message_parameters = {}
                version, message = uri.split('.', 1)
                path = '/' + version + '/' + message
                cls_message_parameters = message_content.parameters
            if isinstance(func, str):
                func_args = None
            else:
                func_args = parse_function(func, None, database, allow_kwargs)
            method = http_type
        local_wrapper = {'func': func,
                         'uri': uri,
                         'database': database,
                         'method': method,
                         'parameters_type': parameters_type,
                         'add_uri_in_response': add_uri_in_response,
                         'func_args': func_args}
        if lparam:
            local_wrapper['param'] = lparam
        elif param is not undefined:
            local_wrapper['param'] = param
        if cls_message_parameters is not None:
            local_wrapper['message_parameters'] = cls_message_parameters
        if isinstance(func, str):
            name = func
            handle = name
        else:
            name = func.__name__
            handle = type(name, (aiohttp_wrapper,), local_wrapper)

        aiohttp_wrapper.httphandlers.append((path, handle, method))
    return decorator
