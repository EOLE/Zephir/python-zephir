from aiohttp.web import Application, Response
from json import dumps


from ..config import HTTP_PORT
from .register import aiohttp_wrapper


class HttpController:
    async def init_http(self, loop):
        app = Application(loop=loop)
        app.router.add_get('/ping', http_ping)
        for path, handle, method in aiohttp_wrapper.httphandlers:
            if callable(handle):
                name = handle.__name__
            else:
                name = handle
            print('{}: HTTP ({})   “{}” handled by endpoint “{}”'.format(self.__class__.__name__, method, path, name))
            if method in ['get', 'get_file']:
                app.router.add_get(path, handle)
            elif method == 'post':
                app.router.add_post(path, handle)
            elif method == 'put':
                app.router.add_put(path, handle)
            elif method == 'static':
                if callable(handle):
                    # static file
                    app.router.add_get(path, handle)
                else:
                    # static dir
                    app.router.add_static(path, handle)
            else:
                app.router.add_post(path, handle)

        return await loop.create_server(app.make_handler(), '*', HTTP_PORT)


def http_ping(*args, **kwargs):
    return Response(text='pong')
