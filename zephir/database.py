import psycopg2

KEY_DATABASE_NAME = 'dbname'
KEY_DATABASE_USER = 'user'
KEY_DATABASE_PASSWORD = 'password'
KEY_DATABASE_HOST = 'host'


def connect(conf, database_options=None):
    if database_options is None:
        option = conf.option['database']
        database_options = {
                            'host': option[KEY_DATABASE_HOST],
                            'dbname': option[KEY_DATABASE_NAME],
                            'user': option[KEY_DATABASE_USER],
                            'password': option[KEY_DATABASE_PASSWORD]

                }
    if not database_options['host']:
        raise Exception('cannot find postgresql')
    return psycopg2.connect(**database_options)
