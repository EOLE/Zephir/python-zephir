from inspect import signature


from .i18n import _


CONTEXT_KEYS = {'_request_id', '_session_user'}


class Undefined:
    pass

undefined = Undefined()


def parse_function(func, message_parameters, database, allow_kwargs=False):
    """verifying arity of a function with message
    """
    args = []
    # first argument is self, second is cursor if database is True
    first_argument_index = 2 if database is True else 1
    for param in list(signature(func).parameters.values())[first_argument_index:]:
        if param.kind == param.VAR_POSITIONAL:
            raise Exception(_('function {} must not *args').format(func.__name__))
        elif param.kind == param.VAR_KEYWORD:
            if allow_kwargs:
                # all arguments are allowed
                return None
            raise Exception(_('function {} must not **kwargs').format(func.__name__))
        elif param.default is param.empty:
            if message_parameters is not None and param.name not in message_parameters and param.name not in CONTEXT_KEYS:
                raise Exception(_('function {} have unknown parameter {}').format(func.__name__, param.name))
            args.append(param.name)
        else:
            raise Exception(_('function {} must not have parameter {} with default value').format(func.__name__, param.name))
    return args


def check_params(message_parameters, kwargs_keys, add_uri_in_response=False):
    message_keys = set(message_parameters.keys())
    unknown_kwargs = kwargs_keys - message_keys - CONTEXT_KEYS
    if add_uri_in_response:
        unknown_kwargs -= {'uri'}
    if unknown_kwargs:
        raise Exception(_('unknown parameters {}').format(unknown_kwargs))


def validate_argument(uri, key, value, key_type):
    if key_type.startswith('[]'):
        assert isinstance(value, list), "{} must be a list".format(key)
        key_type2 = key_type[2:]
        for val in value:
            validate_argument(uri, key, val, key_type2)
    elif key_type == 'Number':
        assert isinstance(value, (int, float)), "{} must be a Number ({})".format(key, uri, value)
    elif key_type == 'String':
        assert isinstance(value, str), "{} must be a String ({})".format(key, uri, value)
    elif key_type == 'Boolean':
        assert isinstance(value, bool), "{} must be a Boolean ({})".format(key, uri, value)
    elif key_type == 'Dict':
        assert isinstance(value, dict), "'{}' must be a Dict in '{}' ({})".format(key, uri, value)
    else:
        raise TypeError(_('unknown type {}').format(key_type))


def build_arguments(uri, message_parameters, func_args, kwargs, add_uri_in_response=False):
    """build arguments usable for a function
    """
    kwargs_keys = set(kwargs.keys())
    if message_parameters:
        check_params(message_parameters, kwargs_keys, add_uri_in_response)
    returned_args = {}
    if message_parameters and func_args is not None:
        # set default value if param not set in kwargs
        for name, parameter in message_parameters.items():
            if name in func_args and name not in kwargs:
                if not hasattr(parameter, 'default'):
                    raise Exception(_('parameter {} is mandatory').format(name))
                returned_args[name] = parameter.default
    for key, value in kwargs.items():
        # remove argument not present in func_args
        if func_args is None or key in func_args:
            # check type
            if message_parameters and key not in CONTEXT_KEYS and (not add_uri_in_response or key != 'uri'):
                key_type = message_parameters[key].type
                validate_argument(uri, key, value, key_type)
            returned_args[key] = value
    return returned_args

