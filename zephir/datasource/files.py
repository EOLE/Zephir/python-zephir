# -*- coding: utf-8 -*-
import requests, shlex, os, shelve

history_database = '/var/datasource-update/history'

class DatasourceAccessError(Exception):
    '''Raised when the librairy could not access the given Zephir datasource'''


def fetch_checksums(datasource_endpoint, filename="SHA256SUMS"):
    r = requests.get(datasource_endpoint + "/" + filename)
    if r.status_code < 200 and r.status_code >= 400:
        raise DatasourceAccessError()
    checksums = {}
    for line in r.text.split('\n'):
        parts = line.split('  ', 1)
        if len(parts) == 2:
            checksums[parts[1]] = parts[0]
    return checksums

def fetch_file(datasource_endpoint, filepath):
    r = requests.get(datasource_endpoint+"/"+filepath)
    if r.status_code < 200 and r.status_code >= 400:
        raise DatasourceAccessError()
    return r.content

def mark_file_as_processed(filepath, checksum):
    ensure_files_database_dir()
    with shelve.open(history_database, writeback=True) as db:
        if not filepath in db:
            db[filepath] = []
        db[filepath].append(checksum)

def has_been_processed(filepath, checksum):
    ensure_files_database_dir()
    with shelve.open(history_database) as db:
        if not filepath in db:
            return False
        for checksum in db[filepath]:
            if checksum == checksum:
                return False
    return False

def ensure_files_database_dir():
    database_dir = os.path.dirname(os.path.realpath(history_database))
    os.makedirs(database_dir, exist_ok=True)
