import asyncio
import socket
import sys
from aiohttp import ClientSession
from autobahn.asyncio.wamp import ApplicationSession
from autobahn.wamp.exception import ApplicationError
from autobahn.wamp.protocol import Registration, Subscription
import casbin

from ..config import DEBUG
from ..i18n import _
from ..message import get_message
from .register import register
from ..http.register import aiohttp_wrapper


class WampController(ApplicationSession):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if 'policy' not in dir(self):
            self.policy = casbin.Enforcer('/etc/acl/model.conf','/etc/acl/policy.csv')

    async def onJoin(self, details):
        # a remote procedure; see frontend.py for a Python front-end
        # that calls this. Any language with WAMP bindings can now call
        # this procedure if its connected to the same router and realm.

        # register all methods on this object decorated with "@wamp.register"
        # as a RPC endpoint
        ##
        results = await self.register(self)
        for res in results:
            if isinstance(res, Registration):
                # res is an Registration instance
                log_msg = '{}: procedure “{}” handled by endpoint “{}” with registration ID “{}”'
                if res.endpoint.obj:
                    callback_name = '{}.{}'.format(res.endpoint.obj.__class__.__name__, res.endpoint.fn.__name__)
                else:
                    callback_name = res.endpoint.fn.__name__
                print(log_msg.format(self.__class__.__name__, res.procedure, callback_name, res.id))
            else:
                # res is an Failure instance
                log_msg = '{}: failed to register procedure: {}'
                print(log_msg.format(self.__class__.__name__, res))
                exit(1)

        # subscribe all methods on this object decorated with "@wamp.subscribe"
        # as PubSub event handlers
        results = await self.subscribe(self)
        for res in results:
            if isinstance(res, Subscription):
                # res is an Subscription instance
                msg = "{}: topic     “{}” subscribed by handler “{}” with subscription ID “{}”"
                if res.handler.obj:
                    handler_name = '{}.{}'.format(res.handler.obj.__class__.__name__, res.handler.fn.__name__)
                else:
                    handler_name = res.handler.fn.__name__
                print(msg.format(self.__class__.__name__, res.topic, handler_name, res.id))
            else:
                # res is an Failure instance
                log_msg = '{}: failed to subscribe topic: {}'
                print(log_msg.format(self.__class__.__name__, res))
                exit(1)
        name = self.options.option['default']['name']
        ip = socket.gethostbyname(socket.gethostname())
        await asyncio.sleep(1)
        if aiohttp_wrapper.database:
            #if database is needed, waiting for it
            itr = 0
            if DEBUG:
                print('connecting to the database')
            while itr < 18:
                try:
                    if DEBUG:
                        print('trying to connect to db {}'.format(itr))
                    await self.create_db()
                except Exception as err:
                    if DEBUG:
                        import traceback
                        traceback.print_exc()
                        print('error', str(err))
                    itr += 1
                    await asyncio.sleep(4)
                else:
                    if DEBUG:
                        print('connected')
                    break
        self.publish('v1.zephir.services.event', name=name, status='start', ip=ip)

    async def onLeave(self, details):
        # Not working ?
        name = self.options.option['default']['name']
        ip = socket.gethostbyname(socket.gethostname())
        self.publish('v1.zephir.services.event', name=name, status='stop', ip=ip)

    async def onDisconnect(self):
        # Here we apply the "fail fast" philosophy.
        # The service supervisor will handle the service restart.
        print("transport lost. exiting")
        sys.exit(1)

    async def call(self, uri, **kwargs):
        message_content = get_message(uri)
        if message_content.pattern != 'rpc':
            raise TypeError(_('call only usable for rpc pattern for {}').format(uri))
        result = await super().call(uri, **kwargs)
        if not isinstance(result, dict):
            raise ValueError(_('call to "{}" do not return a dict').format(uri))
        if 'response' not in result.keys():
            raise ValueError(_('call to "{}" return a dict without mandatory key "response"').format(uri))
        dict_result = result['response']

        message_parameters = message_content.response.parameters
        if message_parameters is not None:
            for param, options in  message_parameters.items():
                if options.type == 'File' and param in dict_result:
                    async with ClientSession() as session:
                        async with session.get(dict_result[param]) as resp:
                            if resp.status != 200:
                                content = await resp.content.read()
                                raise ApplicationError('zephir.error.exception', reason='unable to get {} content: {}'.format(dict_result[param], content))
                            dict_result[param] = await resp.text()
        elif message_content.response.type == 'File' and param in dict_result:
            async with ClientSession() as session:
                async with session.get(dict_result[param]) as resp:
                    if resp.status != 200:
                        content = await resp.content.read()
                        raise ApplicationError('zephir.error.exception', reason='unable to get {} content: {}'.format(dict_result[param], content))
                    dict_result = await resp.text()
        return dict_result

    @register('v1.zephir.services.available', None)
    async def is_available(self, name):
        if name == [None, self.options.option['default']['name']]:
            ip = socket.gethostbyname(socket.gethostname())
            self.publish('v1.zephir.services.event', name=name, status='started', ip=ip)
