import socket
from binascii import hexlify
from os import urandom
from autobahn.wamp import register as wampregister
from autobahn.wamp import subscribe as wampsubscribe
from autobahn.wamp.types import CallResult, RegisterOptions
from functools import wraps
from autobahn.wamp.exception import ApplicationError
from psycopg2.extras import DictCursor


from ..http.register import aiohttp_wrapper
from ..util import undefined, parse_function, build_arguments, validate_argument
from ..message import get_message
from ..i18n import _
from ..config import DEBUG, HTTP_PORT


def validate_dict(uri, ret, response_parameters, has_file, required):
    if not isinstance(ret, dict):
        raise ApplicationError('zephir.error.exception', reason=_('response for "{}" must be a dict').format(uri))
    params = set()
    for param, options in response_parameters.items():
        if param not in ret:
            if param in required:
                raise ApplicationError('zephir.error.exception', reason=_('missing "{}" in response for "{}"').format(param, uri))
            continue
        key_type = options.type
        if options.type == 'File':
            has_file = True
        else:
            try:
                validate_argument(uri, param, ret[param], key_type)
            except AssertionError as err:
                raise ValueError('params for uri {} are invalid: {}'.format(param, uri, str(err)))
            if hasattr(options, 'properties'):
                has_file = validate_dict(uri, ret[param], options.properties, has_file, options.required)
        params.add(param)
    extra_param = params - set(ret.keys())
    if extra_param:
        raise ApplicationError('zephir.error.exception', reason=_('not allowed parameters "{}" in response for "{}"').format(extra_param, uri))
    return has_file


class WrapperWampController:
    def __init__(self, wamp_controller, origin_kwargs):
        self.wamp_controller = wamp_controller
        self.origin_kwargs = origin_kwargs

    def __getattr__(self, key):
        return getattr(self.wamp_controller, key)

    def publish(self, queue, **details):
        if '_session_user' not in details and '_session_user' in self.origin_kwargs:
            details['_session_user'] = self.origin_kwargs['_session_user']
        self.wamp_controller.publish(queue, **details)

    async def call(self, uri, **kwargs):
        if '_session_user' not in kwargs and '_session_user' in self.origin_kwargs:
            kwargs['_session_user'] = self.origin_kwargs['_session_user']
        return await self.wamp_controller.call(uri, **kwargs)


def register(uri: str,
             notification_uri: str=undefined,
             database: bool=False,
             wamp_invoke: str=None,
             profil_adapter: str=None):
    if database:
        aiohttp_wrapper.database = True
    def decorator(func):
        message_content = get_message(uri)
        # test if notification_uri is a valid message
        if notification_uri not in [undefined, None]:
            get_message(notification_uri)

        message_parameters = dict(message_content.parameters.items())
        func_args = parse_function(func, message_parameters.keys(), database)
        options = None
        if message_content.pattern == 'event':
            wampdecorator = wampsubscribe
            if wamp_invoke is not None:
                raise TypeError(_('the event message "{}" cannot have wamp_invoke').format(uri))
        elif message_content.pattern == 'rpc':
            wampdecorator = wampregister
            if wamp_invoke is None:
                lwamp_invoke = 'roundrobin'
            else:
                lwamp_invoke = wamp_invoke
            options = RegisterOptions(invoke=lwamp_invoke)
            if message_content.response.parameters:
                for name, opts in message_content.response.parameters.items():
                    if opts.type == 'File':
                        aiohttp_wrapper.wamp_messages.add((uri, name))
        else:
            raise TypeError(_("unknown pattern {} for {}").format(message_content.pattern, uri))

        @wraps(func)
        @wampdecorator(uri, options=options)
        async def wrapper(self, *args, **kwargs):
            if args:
                raise ApplicationError('zephir.error.exception', reason=str('args ({}) are not allowed'.format(args)))
            try:
                parsed_arguments = build_arguments(uri, message_parameters, func_args, kwargs)
            except Exception as err:
                raise ApplicationError('zephir.error.exception', reason=str('unable to load arguments {} for "{}" when executed  {}: {}'.format(kwargs, uri, func.__name__, str(err))))
            if DEBUG:
                print('call crossbar - uri: {}: param: {}'.format(uri, parsed_arguments))

            _profil = 'root'

            try:
                if self.policy is not None:
                    if '_session_user' not in kwargs:
                        _profil = 'root'
                    else:
                        if 'profil' in kwargs['_session_user']:
                            _profil = kwargs['_session_user']['profil']
                        else: #FIXME remove auto root
                            _profil = 'root'
            except TypeError as err:
                pass

            if database is True:
                # if connexion is lost during query, restart one time before leave with an error
                retry = 0
                while True:
                    if retry and DEBUG:
                        print('retrying database connexion')
                    try:
                        conn = None
                        conn = await self.connect_db()
                        with conn.cursor(cursor_factory=DictCursor) as cursor:
                            if self.policy is not None and message_content.pattern != 'event':
                                if profil_adapter is not None:
                                    _profil = getattr(self, profil_adapter)(cursor, uri, parsed_arguments, kwargs)
                                check_policy(self.policy, _profil, uri, 'allowed')
                            ret = await func(WrapperWampController(self, kwargs), cursor, **parsed_arguments)
                    except Exception as err:
                        if conn:
                            if conn.closed:
                                retry += 1
                                if retry == 1:
                                    continue
                            else:
                                conn.rollback()
                        if isinstance(err, MessageNotAllowed):
                            raise ApplicationError('zephir.error.notallowed', reason=str(err))
                        if isinstance(err, ApplicationError):
                            raise err
                        raise ApplicationError('zephir.error.exception', reason=str(err))
                    else:
                        conn.commit()
                        break
            else:
                try:
                    if self.policy is not None and message_content.pattern != 'event':
                        if profil_adapter is not None:
                            _profil = getattr(self, profil_adapter)(uri, parsed_arguments, kwargs)
                        check_policy(self.policy, _profil, uri, 'allowed')
                    ret = await func(WrapperWampController(self, kwargs), **parsed_arguments)
                except MessageNotAllowed as err:
                    raise ApplicationError('zephir.error.notallowed', reason=str(err))
                except ApplicationError as err:
                    raise err
                except Exception as err:
                    raise ApplicationError('zephir.error.exception', reason=str(err))

            if notification_uri not in [undefined, None]:
                if '_session_user' not in ret and '_session_user' in kwargs:
                    ret['_session_user'] = kwargs['_session_user']
                self.publish(notification_uri, **ret)

            if message_content.pattern == 'rpc':
                response_parameters = message_content.response.parameters
                has_file = False
                if response_parameters is None:
                    validate_argument(uri, 'response', ret, message_content.response.type)
                else:
                    required = message_content.response.required
                    if message_content.response.type == 'Dict':
                        try:
                            has_file = validate_dict(uri, ret, response_parameters, has_file, required)
                        except Exception as err:
                            raise ApplicationError('zephir.error.exception', reason=_('response not valid for "{}": {}').format(uri, str(err)))
                    elif message_content.response.type == '[]Dict':
                        if not isinstance(ret, list):
                            raise ApplicationError('zephir.error.exception', reason=_('response for "{}" must be a list of dict').format(uri))
                        try:
                            for iter_ret in ret:
                                has_file = validate_dict(uri, iter_ret, response_parameters, has_file, required)
                        except Exception as err:
                            raise ApplicationError('zephir.error.exception', reason=_('response not valid for "{}": {}').format(uri, str(err)))
                    else:
                        raise Exception(_('unknown format {}').format(message_content.response.type))
                if has_file:
                    if response_parameters is not None:
                        for param, options in response_parameters.items():
                            if options.type == 'File' and param in ret:
                                secret = hexlify(urandom(24)).decode()
                                self.http_sessions[secret] = ret[param]
                                ip = socket.gethostbyname(socket.gethostname())
                                url = 'http://{}:{}/{}/{}/{}'.format(ip, HTTP_PORT, uri, param, secret)
                                ret[param] = url
                    else:
                        if message_content.response.type == 'File':
                            secret = hexlify(urandom(24)).decode()
                            self.http_sessions[secret] = ret
                            ip = socket.gethostbyname(socket.gethostname())
                            url = 'http://{}:{}/{}/{}/{}'.format(ip, HTTP_PORT, uri, param, secret)
                            ret[param] = url
                return CallResult({'response': ret})
        return wrapper
    return decorator


class MessageNotAllowed(Exception):
    pass


def check_policy(policy, profil, uri, action):

    if profil is None or not policy.enforce(profil, uri, action):
        raise MessageNotAllowed(_("Not allowed to access to message {}").format(uri))
