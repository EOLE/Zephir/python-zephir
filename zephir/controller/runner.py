import asyncio
import time, signal
from os import unlink, getpid, makedirs
from os.path import join, exists
from autobahn.asyncio.wamp import ApplicationRunner
from autobahn.wamp.types import ComponentConfig


from ..i18n import _
from ..config import ServiceConfig
from ..http.register import aiohttp_wrapper
from ..coroutine.register import run_registered_coroutines


CTRL_SEMAPHORE = '/var/run/ctrl_semaphore'


def run(cls):
    config = ServiceConfig()

    # Configuration file does not exists -> wait
    while not config.is_crossbar_available():
        print(_("Crossbar service unavailable. Waiting..."))
        config.load_config()
        time.sleep(5)

    # Prepare controller semaphore
    if not exists(CTRL_SEMAPHORE):
        makedirs(CTRL_SEMAPHORE)
    pid = str(getpid())
    semaphore_path = join(CTRL_SEMAPHORE, pid)

    # Reload configuration on SIGUSR2
    # See ContainerPilot configuration
    signal.signal(signal.SIGUSR2, config.reload_config)

    # Load config must be after signal register
    # to ensure conf file is not modified between initial loading and SIGUSR2 registration
    config.load_config()

    # Write semaphore : controller is ready
    with open(semaphore_path, 'w') as semaphore:
        semaphore.write(pid)

    # init asyncio loop
    loop = asyncio.get_event_loop()

    # initialize WAMP session
    runner = ApplicationRunner(
        config.option['crossbar']['host'],
        config.option['crossbar']['realm'],
    )
    controller = cls(ComponentConfig(runner.realm))
    cls.options = config
    loop.run_until_complete(runner.run(controller, start_loop=False))

    # validate if some message are missing
    missing_http_messages = aiohttp_wrapper.wamp_messages - aiohttp_wrapper.http_messages
    if missing_http_messages:
        msg = []
        for missing_uri, missing_param in missing_http_messages:
            msg.append(_('no message "{}" with param "{}" is registered').format(missing_uri, missing_param))
        raise Exception(_(', '.join(msg)))
    missing_wamp_messages = aiohttp_wrapper.http_messages - aiohttp_wrapper.wamp_messages
    if missing_wamp_messages:
        msg = []
        for missing_uri, missing_param in missing_wamp_messages:
            msg.append(_('message "{}" with param "{}" is registered, but any message without param found').format(missing_uri, missing_param))
        raise Exception(', '.join(msg))
    del aiohttp_wrapper.wamp_messages
    del aiohttp_wrapper.http_messages

    # initialize HTTP server if needed
    if aiohttp_wrapper.httphandlers:
        aiohttp_wrapper.injected_self = controller
        loop.run_until_complete(controller.init_http(loop))

    # Run controller's registered coroutines
    run_registered_coroutines(controller, loop)

    # start the loop
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass

    unlink(semaphore_path)
