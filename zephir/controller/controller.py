from psycopg2 import connect
from psycopg2.extras import DictCursor
from subprocess import run as sp_run


from ..config import KEY_DATABASE_NAME
from ..i18n import _
from ..http.controller import HttpController
from ..wamp.controller import WampController


class ZephirDatabase:
    def get_dbname(self):
        return self.options.option['database'][KEY_DATABASE_NAME]

    async def create_db(self):
        dbname = self.get_dbname()
        if await self.call('v1.vault.database.create', name=dbname):
            conn_parameters = await self.call('v1.vault.database.get', name=dbname)
            host = conn_parameters['host']
            user = conn_parameters['username']
            password = conn_parameters['password']
            db_uri = "db:postgresql://{}:{}@{}/{}".format(user, password, host, dbname)
            sp_run(['sqitch', '--engine', 'postgresql', '--top-dir', '/migrations', 'deploy', db_uri], check=True)
            # reassign all object (almost database creation) to user xxx_admin
            # without that we cannot suppress current user and recreate new one
            with connect(host=host, dbname=dbname, user=user, password=password) as conn:
                with conn.cursor(cursor_factory=DictCursor) as cursor:
                    cursor.execute('REASSIGN OWNED BY "{}" to "{}_admin"'.format(user, dbname))
            await self.database_created()
        self.created = True

    async def connect_db(self):
        """
        Returns a connection to the service database
        """
        if not self.created:
            raise Exception(_('please create database first'))
        if self.conn is not None and self.conn.closed:
            self.conn = None
        if self.conn is None:
            dbname = self.options.option['database'][KEY_DATABASE_NAME]
            conn_parameters = await self.call('v1.vault.database.get', name=dbname)
            host = conn_parameters['host']
            user = conn_parameters['username']
            password = conn_parameters['password']
            self.conn = connect(host=host,
                                dbname=dbname,
                                user=user,
                                password=password)
        return self.conn

    async def database_created(self):
        pass


class ZephirCommonController(ZephirDatabase, HttpController, WampController):
    conn = None
    http_sessions = {}
    created = False
