import configparser
from os import environ

HTTP_PORT = '8080'

MESSAGE_ROOT_PATH = '/srv/messages'
DATABASE_NAME = 'database'
KEY_DATABASE_NAME = 'dbname'
#FIXME should be in conf file
DEBUG = bool(int(environ.get('DEBUG', 0)))


def get_configuration_path():
    configuration_path = environ.get('services_conf', '/etc/zephir-services.conf')
    return configuration_path


class ServiceConfig():
    """Salt controller configuration

    """
    def __init__(self):
        self.option = None
        self.config_file = get_configuration_path()
        self.load_config()

    def load_config(self):
        """Load service configuration from filesystem

        """
        config = configparser.ConfigParser(allow_no_value=True)
        config.optionxform = str
        with open(self.config_file, "r") as configfile:
            config.read_file(configfile)
            self.option = config

    def reload_config(self, *args):
        """Reload the service configuration
        """
        print('Reloading configuration...')
        self.load_config()

    def is_crossbar_available(self):
        return self.option['crossbar']['host'] and self.option['crossbar']['realm']

    def is_vault_available(self):
        return self.option['vault'].get('host') is not None
